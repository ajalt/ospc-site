from __future__ import division

from flask.ext.login import current_user, login_required
from flask.ext import admin
from flask.ext.admin.contrib import sqla
from flask.ext.admin.contrib.fileadmin import FileAdmin
import wtforms as wtf
from sqlalchemy.sql import func


from ..utils import admin_required
from .. import models
from ..models import Submission
from ..extensions import db

class ContestAdminModelView(sqla.ModelView):
    """All admin views are only accessible to admins."""
    def is_accessible(self):
        return current_user.is_authenticated() and getattr(current_user, 'is_admin', False)

class ContestAdminIndexView(admin.AdminIndexView):
    @admin.expose('/')
    @login_required
    @admin_required
    def index(self):
        num_users =  models.User.query.count()
        total_score_all, count_submissions_all = db.session.query(
            func.sum(Submission.result_score), func.count(Submission.id)).one()
        total_score_pass, count_submissions_pass = db.session.query(
            func.sum(Submission.result_score), func.count(Submission.id)).filter(
                Submission.result == 'Pass').one()

        avg_all = total_score_all / count_submissions_all
        avg_pass = total_score_pass / count_submissions_pass

        return self.render('admin/index.html', num_users=num_users, 
                            count_submissions_all=count_submissions_all, 
                            count_submissions_pass=count_submissions_pass,
                            avg_all=avg_all, avg_pass=avg_pass)

class MailListAdminView(admin.BaseView):
    @admin.expose('/')
    @login_required
    @admin_required
    def index(self):
        emails = [i[0] for i in db.session.query(models.User.email).all()]
        return self.render('admin/mail.html', emails=emails)

    def __init__(self, **kwargs):
        return super(MailListAdminView, self).__init__(name='Mailing List', **kwargs)

class SubmissionAdminView(ContestAdminModelView):
    column_list = ('id', 'problem', 'user', 'timestamp', 'language', 'result', 'result_score')
    column_filters = ('id', 'problem', 'user')
    form_excluded_columns = ('timestamp',)
    column_display_pk = True

    def __init__(self, session, **kwargs):
        # Just call parent class with the Submission model.
        super(SubmissionAdminView, self).__init__(models.Submission, session, **kwargs)

class UserAdminView(ContestAdminModelView):
    column_list = ('name', 'email', 'is_admin', 'password')
    column_filters = ('is_admin',)
    column_searchable_list = ('name', 'email')
    form_excluded_columns = ('password',)
    column_exclude_list = ('password',)

    def __init__(self, session, **kwargs):
        super(UserAdminView, self).__init__(models.User, session, **kwargs)

    def scaffold_form(self):
        # This is called by flask-admin to create the form object 
        form = super(UserAdminView, self).scaffold_form()
        form.new_password = wtf.PasswordField('New Password')
        return form

    def on_model_change(self, form, model, is_created):
        # This is called by flask-admin after the form is submitted and a
        # model is created, but before the transaction is commited.
        if form.new_password:
            model.set_password(form.new_password.data)

class ProblemAdminView(ContestAdminModelView):
    column_list = ('id', 'name')
    column_display_pk = True

    def __init__(self, session, **kwargs):
        super(ProblemAdminView, self).__init__(models.Problem, session, **kwargs)

class ContestStatusAdminView(ContestAdminModelView):
    def __init__(self, session, **kwargs):
        super(ContestStatusAdminView, self).__init__(models.ContestStatus, session, **kwargs)

class SubmissionLanguageAdminView(ContestAdminModelView):
    def __init__(self, session, **kwargs):
        super(SubmissionLanguageAdminView, self).__init__(models.SubmissionLanguage, session, **kwargs)

class ContestFileUploadView(FileAdmin):
    pass
  