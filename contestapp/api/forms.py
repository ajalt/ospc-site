from flask_wtf import Form
from flask_wtf.file import FileField
import wtforms as wtf
from wtforms.validators import InputRequired, NumberRange

from contestapp.extensions import db
from contestapp import models

def make_upload_form(problem_id_=None):
    """Make a form instance with an id field that can be hidden or visible."""
    class UploadForm(Form):
        if problem_id_:
            problem_id = wtf.HiddenField(validators=[InputRequired(), NumberRange(min=1)], 
                                         default=problem_id_)
        else:
            choices = db.session.query(models.Problem.id, models.Problem.name).order_by('name')
            problem_id = wtf.SelectField('Problem', coerce=int, choices=choices)

        ext = db.session.query(models.SubmissionLanguage.file_ext)
        desc = 'Your file must have on the the following extensions: (%s)' % (
                    ', '.join(str(i[0]) for i in ext))
        file = FileField('Source File', validators=[InputRequired()], description=desc)
        submit_button = wtf.SubmitField('Submit')
    return UploadForm()