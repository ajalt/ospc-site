from flask import Blueprint, render_template
from flask.ext.login import current_user, login_required
from sqlalchemy.sql import func

from . import forms
from contestapp.models import User, Problem, Submission, SubmissionLanguage
from contestapp.extensions import login_manager, db
from contestapp.utils import started_contest_required

blueprint = Blueprint('api', __name__, url_prefix='/api', static_folder='../static')

@blueprint.route('/upload')
@blueprint.route('/upload/<problem_id>')
@login_required
@started_contest_required
def api_upload(problem_id=None):
    """Create an embeddable form that will redirect to '/upload'"""
    form = forms.make_upload_form(problem_id)
    return render_template('api/upload.html', form=form)

@blueprint.route('/submissions')
@blueprint.route('/submissions/<problem_id>')
@login_required
@started_contest_required
def submissions(problem_id=None):
    subs = Submission.query.filter_by(user=current_user)
    if problem_id is not None:
        subs = subs.filter(Submission.problem_id == problem_id)
    return render_template('api/submissions.html', submissions=subs)

@blueprint.route('/submissions/text/<submission_id>')
@login_required
@started_contest_required
def submission_text(submission_id):
    problem = Submission.query.get(int(submission_id))
    if problem.user != current_user:
        return ''
    return render_template('api/submission_text.html', problem=problem)

@blueprint.route('/description/<problem_id>')
@started_contest_required
def descriptions(problem_id):
    if problem_id == u'null':
        return ''
    return Problem.query.get(int(problem_id)).description

@blueprint.route('/scoreboard/<problem_id>')
@started_contest_required
def scoreboard(problem_id):
    user_ids = db.session.query(Submission.user_id).filter(Submission.problem_id == problem_id).distinct()
    rows = []

    for user_id in user_ids:
        s = Submission.query.filter(
                Submission.problem_id == problem_id, Submission.user_id == user_id[0]).order_by(
                    Submission.result_score.desc(), Submission.timestamp).first()
        rows.append((s.user.name, s.result_score or 0, s.language.name, s.timestamp))
    rows.sort(key=(lambda x: (x[1],x[3])), reverse=True)
    return render_template('api/scoreboard.html', rows=rows)
