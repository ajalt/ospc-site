"""The app module contains the app factory function."""
import os 

from flask import Flask, url_for

from . import extensions
from . import settings

from . import api, public, user
from .admin import views as av

def create_app(config=settings.ProdConfig):
    app = Flask(__name__)
    app.config.from_object(config)
    register_admin(extensions.admin)
    register_extenstions(app)
    register_blueprints(app)
    return app

def register_extenstions(app):
    extensions.bootstrap.init_app(app)
    extensions.db.init_app(app)
    extensions.login_manager.init_app(app)
    extensions.mail.init_app(app)
    extensions.migrate.init_app(app, extensions.db)
    extensions.admin.init_app(app)

def register_blueprints(app):
    app.register_blueprint(api.views.blueprint)
    app.register_blueprint(public.views.blueprint)
    app.register_blueprint(user.views.blueprint)

def register_admin(admin):
    # Flask-Admin requires that the index view be set in the constructor. That
    # doesn't work outside of single-module apps, so a workaround is to delete
    # the default index before adding our own. This must be done before caling
    # admin.init_app()
    admin._views = []
    admin._menu = []
    admin.add_view(av.ContestAdminIndexView())
    admin.add_view(av.MailListAdminView())
    admin.add_view(av.ContestStatusAdminView(extensions.db.session))
    admin.add_view(av.UserAdminView(extensions.db.session, endpoint='user_admin'))
    admin.add_view(av.SubmissionLanguageAdminView(extensions.db.session))
    admin.add_view(av.ProblemAdminView(extensions.db.session))
    admin.add_view(av.SubmissionAdminView(extensions.db.session))

    path = os.path.join(os.path.dirname(__file__), 'static')
    admin.add_view(av.ContestFileUploadView(path, name='Static Files'))
