import datetime
import os

import itsdangerous
from flask import current_app
from flask.ext.login import UserMixin
from sqlalchemy import or_, and_
from werkzeug.security import generate_password_hash, check_password_hash

from .extensions import db

class ContestStatus(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    contest_started = db.Column(db.Boolean(), default=True)
    push_notification_hostname = db.Column(db.String(100))
    push_notification_port = db.Column(db.Integer())

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    is_admin = db.Column(db.Boolean(), default=False)
    name = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)

    def __init__(self, name, email, plaintext_pass):
        self.name = name
        self.email = email
        self.set_password(plaintext_pass)

    def set_password(self, plaintext_pass):
        self.password = generate_password_hash(plaintext_pass)

    def verify_password(self, plaintext_pass):
        return check_password_hash(self.password, plaintext_pass)

    def __repr__(self):
        return "<User '%s'>" % self.name

    def __str__(self):
        return self.name

    @classmethod
    def from_email(cls, email):
        """Return a user object for a given email."""
        return cls.query.filter(User.email == email).first()

    @classmethod
    def from_name_or_email(cls, name_or_email):
        """Return a user object for a given username or email."""
        return cls.query.filter(or_(User.name == name_or_email, User.email == name_or_email)).first()

    @classmethod
    def authenticate(cls, name_or_email, plaintext_pass):
        """Return a User object given valid credentials.

        The login parameter may be either the username or password.
        None is returned on failure.
        """
        user = cls.from_name_or_email(name_or_email)
        if user and user.verify_password(plaintext_pass):
            return user
        return None

    def get_token(self, tag):
        """Return a token that can be used to validate or reset a user.

        The tag is a plain string that is used as a namespace. It must also be
        passed to from_token."""
        serialzer = itsdangerous.URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        return serialzer.dumps(self.id, salt=tag)

    @classmethod
    def from_token(cls, token, tag, max_age=None):
        """Given a token, return the user for that token."""
        serialzer = itsdangerous.URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        try:
            id = serialzer.loads(token, max_age=max_age, salt=tag)
        except itsdangerous.BadData:
            return None
        return cls.query.get(int(id))


class Problem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    description = db.Column(db.Text())

    def __repr__(self):
        return "<Problem '%s'>" % self.name

    def __str__(self):
        return self.name

class SubmissionLanguage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    file_ext = db.Column(db.String(3), unique=True, nullable=False)

    def __repr__(self):
        return "<SubmissionLanguage '%s (.%s)'>" % (self.name, self.file_ext)

    def __str__(self):
        return self.name

    @classmethod
    def fromfile(cls, filename):
        """Load an instance based on the extension of a file name."""
        ext = os.path.splitext(filename)[1].lstrip('.')
        return cls.query.filter(cls.file_ext == ext).one()

class Submission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    file_contents = db.Column(db.Text, nullable=False)
    language_id = db.Column(db.Integer, db.ForeignKey('submission_language.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'), nullable=False)
    result = db.Column(db.Enum('Pending', 'Pass', 'Fail', 'Error', name='result_type'), 
                        nullable=False, default='Pending')
    compile_output = db.Column(db.Text)
    run_output = db.Column(db.Text)
    grader_output = db.Column(db.Text)
    result_score = db.Column(db.Integer)

    language = db.relationship('SubmissionLanguage')
    user = db.relationship('User')
    problem = db.relationship('Problem')
