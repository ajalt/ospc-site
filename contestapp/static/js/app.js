var s,
App = {
    settings: {
        displayArea: $('#display-area'),
        problemSelect: $('#problem-select'),
        descriptionButton: $('#description-btn'),
        scoreboardButton: $('#scoreboard-btn'),
        submissionsButton: $('#submissions-btn'),
        submitButton: $('#submit-btn'),
        submissionTextModal: $('#submission-text-modal'),
        submissionTextModalBody: $('#submission-text-modal .modal-body'),
        lastClicked: $('#description-btn'),
    },

    init: function() {
        s = this.settings;
        this.bindUIActions();
        this.updateDisplay();
    },

    bindUIActions: function() {
        s.descriptionButton.click(App.handleButtonClick(App.showDescription));
        s.scoreboardButton.click(App.handleButtonClick(App.showScoreboard));
        s.submissionsButton.click(App.handleButtonClick(App.showSubmissions));
        s.submitButton.click(App.handleButtonClick(App.showSubmit));
        s.problemSelect.change(App.updateDisplay);
    },

    getSubmissionURI: function(id) {
        var base = '/api/submissions';
        if(typeof(id)==='undefined') 
            return base;
        return base + '/' + id;
    },

    loadNoCache: function(elem, url, success) {
        $.ajax(url, {
            dataType: 'html',
            cache: false,
            success: function(data) {
                elem.html(data);
                success();
            }
        });
    },

    getActiveProblemId: function() {
        return s.problemSelect.val();
    },

    updateDisplay: function() {
        s.lastClicked.click();
    },

    handleButtonClick: function(displayCallback) {
        return function(e) {
            $('.content callback.active').removeClass('active');
            $(this).addClass('active');
            s.lastClicked = this;
            return displayCallback();
        }
    },

    showSubmissionText: function(e) {
        id = $(this).data('submissionId');
        App.loadNoCache(s.submissionTextModalBody, '/api/submissions/text/' + id, function() {
            s.submissionTextModal.modal('show');
        });
    },

    showDescription: function(e) {
        s.displayArea.load('/api/description/' + App.getActiveProblemId(), function() {
            // Rerun math typesetting and  syntax highlighting on the current
            // description
            Prism.highlightAll(); 
            MathJax.Hub.Typeset();
        });
    },

    showScoreboard: function(e) {
        App.loadNoCache(s.displayArea, '/api/scoreboard/' + App.getActiveProblemId());
    },

    showSubmissions: function(e) {
        App.loadNoCache(s.displayArea, App.getSubmissionURI(App.getActiveProblemId()), function() {
            $('tr[data-submission-id]').click(App.showSubmissionText);
        });
    },

    showSubmit: function(e) {
        s.displayArea.load('/api/upload/' + App.getActiveProblemId());
    },
};


(function() {
    App.init();
})();