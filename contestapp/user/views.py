from sqlalchemy.sql import func
from sqlalchemy.orm.exc import NoResultFound
from flask import Blueprint, request, redirect, url_for, current_app, render_template, flash
from flask.ext.login import current_user, login_required

from ..api.forms import make_upload_form
from ..models import User, Problem, Submission, SubmissionLanguage
from ..extensions import login_manager, db
from ..utils import started_contest_required, push_notification
from . import forms

blueprint = Blueprint('user', __name__, static_folder='../static')

@login_manager.user_loader
def load_user(id):
    """Used by Flask-Login to load a User model for a session that's logged in"""
    return User.query.get(int(id))

@blueprint.route('/problems')
@login_required
@started_contest_required
def problems():
    problems = Problem.query.all()
    return render_template('user/problems.html', problems=problems)

@blueprint.route('/coming_soon')
@login_required
def coming_soon():
    return render_template('user/coming_soon.html')

@blueprint.route('/upload', methods=['GET', 'POST'])
@login_required
@started_contest_required
def upload():
    form = make_upload_form()

    if form.validate_on_submit() and request.files['file']:
        file_contents = request.files['file'].read()
        try:
            language = SubmissionLanguage.fromfile(request.files['file'].filename)
        except NoResultFound:
            valid = ', '.join(str(l.file_ext) for l in SubmissionLanguage.query.all())
            msg = 'Invalid file format. Must be one of (%s).' % valid
            flash(msg, 'danger')
            return render_template('user/upload.html', form=form)
        else:
            submission = Submission(file_contents=file_contents, language=language,
                                    user=current_user, problem_id=int(form.problem_id.data))
            db.session.add(submission)
            db.session.commit()
            push_notification(submission.id)
            flash('Problem submitted.', 'success')
            return redirect(url_for('user.problems', id=form.problem_id.data)) 
    return render_template('user/upload.html', form=form)

@blueprint.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    profile_form = forms.UpdateProfileForm(prefix='profile_form')
    password_form = forms.UpdatePasswordForm(prefix='password_form')


    if profile_form.validate_on_submit() and profile_form.submit_button.data:
        current_user.email = profile_form.email.data
        db.session.commit()
        flash('Profile updated.', 'success')
    elif password_form.validate_on_submit() and password_form.submit_button.data:
        # Manually clear validation from profile_form, since it wasn't submitted
        profile_form.errors.clear()
        for field in profile_form:
            del field.errors[:]

        current_user.set_password(password_form.new_password.data)
        db.session.commit()
        flash('Password updated.', 'success')

    profile_form.email.data = current_user.email

    return render_template('user/profile.html', profile_form=profile_form, password_form=password_form)



