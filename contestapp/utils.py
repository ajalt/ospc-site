"""Helper functions and decorators."""
import functools
import socket

from flask import redirect, url_for, current_app, flash
from flask.ext.login import current_user

from .models import ContestStatus

def started_contest_required(f):
    """A decorator that will redirect to a landing page if the contest has not started."""
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        if ContestStatus.query.one().contest_started or current_user.is_admin:
            return f(*args, **kwargs)
        return redirect(url_for('user.coming_soon'))
    return wrapper

def admin_required(f):
    """A decorator that will redirect if current user is not an admin."""
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        if getattr(current_user, 'is_admin', False):
            return f(*args, **kwargs)
        flash('You are not authorized to view that page.', 'danger')
        return redirect(url_for('user.problems'))
    return wrapper

def redirect_logged_in(view, **url_for_kwargs):
    """A decorator that will redirect to the given view if current user is logged on."""
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            if current_user.is_authenticated():
                return redirect(url_for(view, **url_for_kwargs))
            return f(*args, **kwargs)
        return wrapper
    return decorator

def push_notification(text):
    """Send a push notification over a socket.

    The PUSH_NOTIFICATION_PORT setting must be configured. 
    Exceptions will be silenced.
    """
    contest_status = ContestStatus.query.one()
    if contest_status.push_notification_port:
        hostname = contest_status.push_notification_hostname or 'localhost'

        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((hostname, contest_status.push_notification_port))
            s.send(str(text))
            s.shutdown(socket.SHUT_RDWR)
            s.close()
        except Exception:
            # In production, the page shouldn't fail to render if the push
            # server isn't running.
            if current_app.config['ENV'] == 'dev':
                raise