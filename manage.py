import os

from flask.ext.script import Manager
from flask.ext.migrate import MigrateCommand

from contestapp.app import create_app
from contestapp.extensions import db
from contestapp.models import SubmissionLanguage, ContestStatus
from contestapp.settings import DevConfig, ProdConfig

if os.environ.get("CONTESTAPP_ENV") == 'prod':
    app = create_app(ProdConfig)
else:
    app = create_app(DevConfig)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

@manager.command
def initdb():
    """Init or reset the database"""
    db.drop_all()
    db.create_all()

    # Create some default submission languages
    for name, ext in (
        ('Python', 'py'),
        ('C', 'c'),
        ('C++', 'cpp'),
        ('Matlab', 'm'),
        ):
        db.session.add(SubmissionLanguage(name=name, file_ext=ext))
    db.session.add(ContestStatus(contest_started=True))
    db.session.commit()


if __name__ == '__main__':
    manager.run()