# Database migrations

This folder holds configurations and scripts for Flask-Migrate. The migration scripts are stored in `./versions`. The initial script is empty. If the database changes, run `manage.py db migrate` to generate a new migration script. Review the script, then run `manage.py db upgrade` to apply the script.